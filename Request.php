<?php
declare(strict_types=1);

namespace BitRock\Routing;

class Request
{
    /**
     * Return array from Json request
     *
     * @return array
     */
    public static function toArray(): array 
    {
        $postData = file_get_contents('php://input');
        $data = json_decode($postData, true);

        return $data;
    } 
}