<?php 

namespace BitRock\Routing;

use BitRock\Container\Container;
use ReflectionMethod;

abstract class AbstractRouter
{
    /** @var Container */
    private $container;

    private $namespace;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * Set namespace for callable actions
     *
     * @param string $namespace
     * @return void
     */
    public function setNamespace(string $namespace): void
    {
        $this->namespace = $namespace;
    }

    abstract public function setNotFound(callable $func);

    abstract protected function triggerNotFound();

    abstract protected function addRoute(string $method, string $route, string $action, array $args);

    /**
     * Create a POST route 
     *
     * @param string $route
     * @param string $action
     * @param array $args
     * @return void
     */
    public function post(string $route, string $action, array $args)
    {
        $this->addRoute('POST', $route, $action, $args);
    }

    /**
     * Create a GET route
     *
     * @param string $route
     * @param string $action
     * @param array $args
     * @return void
     */
    public function get(string $route, string $action, array $args)
    {
        $this->addRoute('GET', $route, $action, $args);
    }

    public function getRequestMethod() 
    {
        return $_SERVER['REQUEST_METHOD'];
    }

    public function getCurrentUri() 
    {
        $uri = rawurldecode($_SERVER['REQUEST_URI']);

        if (strstr($uri, '?')) {
            $uri = substr($uri, 0, strpos($uri, '?'));
        }

        return '/' . trim($uri, '/');
    }

    /**
     * Handle a route
     *
     * @param string $controllerName
     * @param string $methodName
     * @param array $args
     * @return void
     */
    protected function handle(string $controllerName, string $methodName, array $args = null) 
    {
        $reflectionMethod = new ReflectionMethod($controllerName, $methodName);
        $args = $this->resolveParameters($reflectionMethod, $args);

        $reflectionMethod->invokeArgs($this->container->get($controllerName), $args);   
    }

    /**
     * @param ReflectionMethod $reflectionMethod
     * @param array $args
     * @return array
     */
    protected function resolveParameters(ReflectionMethod $reflectionMethod, array $args): array 
    {
        $parameters = $reflectionMethod->getParameters();

        $resolvedParameters = [];
        foreach ($parameters as $key => $parameter) {
            $parameterTypeHint = $parameter->getType() ? $parameter->getType()->getName() : '';

            // Check if parameter typeHint exists in container
            if ($this->container->has($parameterTypeHint)) { 
                $resolvedParameters[$key] = $this->container->get($parameterTypeHint);
            } else {
                $resolvedParameters[$key] = array_shift($args);   
            }
        }
        return $resolvedParameters;
    }

    /**
     * Execute the router
     *
     * @return void
     */
    public function run()
    {
        $method = $this->getRequestMethod();
        $uri = $this->getCurrentUri();

        if (isset($this->routes[$method][$uri])) {
            $this->handle(...array_values($this->routes[$method][$uri]));
        } else {
            $this->triggerNotFound();
        }
    }
}