<?php

namespace BitRock\Routing;

use BitRock\Routing\AbstractRouter;
use Di\Container;
use ReflectionMethod;

class Router extends AbstractRouter
{
    /** @var */
    private $routes;

    /** @var string */
    private $namespace;

    /** @var object|callable */
    private $notFoundFunction;

    /** @var Container */
    private $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    /**
     * @todo parse routes from configuration file
     */
    protected function parseRoutesConfiguration() 
    {
        return;
    }

    public function getCurrentUri() 
    {
        $uri = rawurldecode($_SERVER['REQUEST_URI']);

        if (strstr($uri, '?')) {
            $uri = substr($uri, 0, strpos($uri, '?'));
        }

        return '/' . trim($uri, '/');
    }

    public function getRequestMethod() 
    {
        return $_SERVER['REQUEST_METHOD'];
    }

    /**
     * Create a POST route 
     *
     * @param string $route
     * @param string $action
     * @param array $args
     * @return void
     */
    public function post(string $route, string $action, array $args)
    {
        $this->addRoute('POST', $route, $action, $args);
    }

    /**
     * Create a GET route
     *
     * @param string $route
     * @param string $action
     * @param array $args
     * @return void
     */
    public function get(string $route, string $action, array $args)
    {
        $this->addRoute('GET', $route, $action, $args);
    }

    // TODO: Make put, delete, update methods

    /**
     * Set namespace for callable actions
     *
     * @param string $namespace
     * @return void
     */
    public function setNamespace(string $namespace): void
    {
        $this->namespace = $namespace;
    }

    /**
     * Get namespace for callable actions
     *
     * @return string
     */
    public function getNamespace(): string
    {
        return $this->namespace;
    }

    /**
     * Create a route
     *
     * @param string $method
     * @param string $route
     * @param string $action
     * @param array $args
     * @return void
     */
    protected function addRoute(string $method, string $route, string $action, array $args)
    {
        list($controllerName, $methodName) = explode('@', $action);

        $controllerName = $this->getNamespace() . '\\' . $controllerName;

        $routeParams = [
            'controllerName' => $controllerName,
            'methodName' => $methodName,
            'args' => $args 
        ];

        $this->routes[$method][$route] = $routeParams;

    }

    /**
     * Handle a route
     *
     * @param string $controllerName
     * @param string $methodName
     * @param array $args
     * @return void
     */
    protected function handle(string $controllerName, string $methodName, array $args = null) 
    {
        $reflectionMethod = new ReflectionMethod($controllerName, $methodName);
        $args = $this->resolveParameters($reflectionMethod, $args);

        $reflectionMethod->invokeArgs($this->container->get($controllerName), $args);   
    }

    /**
     * @param ReflectionMethod $reflectionMethod
     * @param array $args
     * @return array
     */
    protected function resolveParameters(ReflectionMethod $reflectionMethod, array $args): array 
    {
        $parameters = $reflectionMethod->getParameters();

        $resolvedParameters = [];
        foreach ($parameters as $key => $parameter) {
            $parameterTypeHint = $parameter->getType() ? $parameter->getType()->getName() : '';

            // Check if parameter typeHint exists in container
            if ($this->container->has($parameterTypeHint)) { 
                $resolvedParameters[$key] = $this->container->get($parameterTypeHint);
            } else {
                $resolvedParameters[$key] = array_shift($args);   
            }
        }
        return $resolvedParameters;
    }

    /**
     * Execute the router
     *
     * @return void
     */
    public function run()
    {
        $method = $this->getRequestMethod();
        $uri = $this->getCurrentUri();

        if (isset($this->routes[$method][$uri])) {
            $this->handle(...array_values($this->routes[$method][$uri]));
        } else {
            $this->triggerNotFound();
        }
    }

    protected function triggerNotFound() 
    {
        if ($this->notFoundFunction) {
            call_user_func($this->notFoundFunction);
        }
    }

    /**
     * Set not found function
     *
     * @param callable $func
     * @return void
     */
    public function setNotFound(callable $func)
    {
        $this->notFoundFunction = $func;
    }
}